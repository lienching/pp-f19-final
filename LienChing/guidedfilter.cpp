#include "guidedfilter.h"

int cols;
int rows;
int I_channels; // 1: gray scale, 3: full color
int p_channels; // 1: gray scale, 3: full color

// Please be noted, I don't apply array_dealloc for every array_alloc, and that may cause memory leak

static void array_alloc(float*** arr_out)
{
    *arr_out = new float*[rows];
    for(int i = 0; i < rows; ++i)
    {
        (*arr_out)[i] = new float[cols];
    }
}

static void array_dealloc(float*** arr_out)
{
    for (int i = 0; i < rows; i++)
    {
        delete[] (*arr_out)[i];
    }
    delete[] (*arr_out);
}

static void array_boxfilter(float **result, float **I, int r)
{
    int rf = r/2;
    int r_square= r*r;

    float **tmp;
    array_alloc(&tmp);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            float acc = 0;
            int b;
            for(int l = (j-rf); l < (j+rf+1); l++)
            {
                if(l<0)
                    acc += I[i][-l];
                else if (l > (cols-1))
                    acc += I[i][2*cols-l-2];
                else
                    acc += I[i][l];
            }
            tmp[i][j] = acc;
        }
    }

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            float acc = 0;
            int b;
            for(int l = (i-rf); l < (i+rf+1); l++)
            {
                if(l<0)
                    acc += tmp[-l][j];
                else if (l > (rows-1))
                    acc += tmp[2*rows-l-2][j];
                else
                    acc += tmp[l][j];
            }
            result[i][j] = acc/r_square;
        }
    }
}

static void array_mul(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] * arr_b[i][j];
        }
    }
}

static void array_div(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] / arr_b[i][j];
        }
    }
}

static void array_sub(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] - arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + b;
        }
    }
}

static cv::Mat convertTo(const cv::Mat &mat, int depth) {
    if (mat.depth() == depth)
        return mat;

    cv::Mat result;
    mat.convertTo(result, depth);
    return result;
}


void GuidedFilterImpl::filter(float **output, float **p) {
    filterSingleChannel(output, p);
}

GuidedFilterMono::GuidedFilterMono(float **origI, int r, double eps) : r(r), eps(eps) {
    I = origI;

    array_alloc(&mean_I);
    array_boxfilter(mean_I, I, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, I);

    float **mean_II;
    array_alloc(&mean_II);
    array_boxfilter(mean_II, tmp, r);

    array_mul(tmp, mean_I, mean_I);

    array_alloc(&var_I);
    array_sub(var_I, mean_II, tmp);
}

void GuidedFilterMono::filterSingleChannel(float **result, float **p) const {
    float **mean_p;
    array_alloc(&mean_p);
    array_boxfilter(mean_p, p, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, p);

    float **mean_Ip;
    array_alloc(&mean_Ip);
    array_boxfilter(mean_Ip, tmp, r);

    array_mul(tmp, mean_I, mean_p);

    float **cov_Ip;
    array_alloc(&cov_Ip);
    array_sub(cov_Ip, mean_Ip, tmp);  // this is the covariance of (I, p) in each local patch.


    float **a;
    array_alloc(&a);
    array_add(tmp, var_I, eps);
    array_div(a, cov_Ip, tmp);  // Eqn. (5) in the paper;

    array_mul(tmp, a, mean_I);

    float **b;
    array_alloc(&b);
    array_sub(b, mean_p, tmp);  // Eqn. (6) in the paper;

    float **mean_a;
    array_alloc(&mean_a);
    array_boxfilter(mean_a, a, r);

    float **mean_b;
    array_alloc(&mean_b);
    array_boxfilter(mean_b, b, r);

    array_mul(tmp, mean_a, I);

    array_add(result, tmp, mean_b);
}

GuidedFilter::GuidedFilter(float **I, int r, double eps) {
    impl_ = new GuidedFilterMono(I, 2 * r + 1, eps);
}

GuidedFilter::~GuidedFilter() {
    delete impl_;
}

void GuidedFilter::filter(float **output, float **p) const {
    impl_->filter(output, p);
}

cv::Mat guidedFilter(cv::Mat &I, cv::Mat &p, int r, double eps, int depth) {
    I = convertTo(I, CV_32F);
    p = convertTo(p, CV_32F);
    cols = I.cols;
    rows = I.rows;
    I_channels = I.channels();
    p_channels = p.channels();

    if (I_channels != 1 || p_channels != 1) {
        std::cerr << "Error, Full color images not suppported" << std::endl;
    }

    cv::Mat output;
    I.copyTo(output);

    // To Array
    float **arr_I, **arr_p, **arr_out;
    array_alloc(&arr_I);
    array_alloc(&arr_p);
    array_alloc(&arr_out);

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_I[i][j] = I.at<float>(i,j);
            arr_p[i][j] = p.at<float>(i,j);
        }
    }

    GuidedFilter(arr_I, r, eps).filter(arr_out, arr_p);

    // To Mat
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            output.at<float>(i,j)=arr_out[i][j];
        }
    }

    array_dealloc(&arr_I);
    array_dealloc(&arr_p);
    array_dealloc(&arr_out);

    output = convertTo(output, depth == -1 ? CV_8U : depth);
    return output;
}
