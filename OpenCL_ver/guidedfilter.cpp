#include <iostream>
#include <omp.h>
#include "guidedfilter.h"
#include <CL/cl.h>
#include <cmath>

int cols;
int rows;
int I_channels; // 1: gray scale, 3: full color
int p_channels; // 1: gray scale, 3: full color

// OpenCV Variable
cl_platform_id cpPlatform;        // OpenCL platform
cl_device_id device_id;           // device ID
cl_context context;               // context
cl_command_queue queue;           // command queue
cl_program program;               // program

cl_kernel kernel_boxfilter;       // kernel
cl_kernel kernel_mat_mul;         // kernel
cl_kernel kernel_mat_div;         // kernel
cl_kernel kernel_mat_sub;         // kernel
cl_kernel kernel_mat_add_mat;     // kernel
cl_kernel kernel_mat_add_const;     // kernel

cl_int err;                       // Error Code

cl_mem d_P;
cl_mem d_I;
cl_mem d_result;
cl_mem d_mean_p;
cl_mem d_tmp;
cl_mem d_mean_Ip;
cl_mem d_cov_Ip;
cl_mem d_a;
cl_mem d_b;
cl_mem d_mean_a;
cl_mem d_mean_b;
cl_mem d_mean_I;
cl_mem d_mean_II;
cl_mem d_var_I;
cl_mem d_boxtmp;

size_t globalSize, localSize;
size_t img_size;

static void array_alloc(float*** arr_out)
{
    *arr_out = new float*[rows];
    for(int i = 0; i < rows; ++i)
    {
        (*arr_out)[i] = new float[cols];
    }
}

static void array_dealloc(float*** arr_out)
{
    for(int i = 0; i < rows; i++)
    {
        delete[] (*arr_out)[i];
    }
    delete[] (*arr_out);
}

static cv::Mat convertTo(const cv::Mat &mat, int depth) {
    if (mat.depth() == depth)
        return mat;

    cv::Mat result;
    mat.convertTo(result, depth);
    return result;
}

void releaseAllOpenCLObj() {
    clReleaseMemObject(d_P);
    clReleaseMemObject(d_I);
    clReleaseMemObject(d_result);
    clReleaseMemObject(d_mean_p);
    clReleaseMemObject(d_tmp);
    clReleaseMemObject(d_mean_Ip);
    clReleaseMemObject(d_cov_Ip);
    clReleaseMemObject(d_a);
    clReleaseMemObject(d_b);
    clReleaseMemObject(d_mean_a);
    clReleaseMemObject(d_mean_b);
    clReleaseMemObject(d_mean_I);
    clReleaseMemObject(d_mean_II);
    clReleaseMemObject(d_var_I);
    clReleaseMemObject(d_boxtmp);

    // Release object
    clReleaseProgram(program);

    clReleaseKernel(kernel_boxfilter);       
    clReleaseKernel(kernel_mat_mul);         
    clReleaseKernel(kernel_mat_div);         
    clReleaseKernel(kernel_mat_sub);         
    clReleaseKernel(kernel_mat_add_mat);     
    clReleaseKernel(kernel_mat_add_const);
    
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

}

void GuidedFilterImpl::filter(float **output, float **p) {
    filterSingleChannel(output, p);
}

GuidedFilterMono::GuidedFilterMono(float **origI, int r, double eps) : r(r), eps(eps) {
    I = origI;

    d_mean_I = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_mean_II = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_var_I = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_tmp = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_I = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, img_size, I, NULL);
    d_boxtmp = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);

    /*
    array_alloc(&mean_I);
    array_boxfilter(mean_I, I, r);
    */
    // Create Execute Pipe
    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_I);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_I);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);


    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    /*
    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, I);
    */
    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_I);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_I);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    /*
    float **mean_II;
    array_alloc(&mean_II);
    array_boxfilter(mean_II, tmp, r);
    */
    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_II);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);


    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    /*
    array_mul(tmp, mean_I, mean_I);
    */
    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_mean_I);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_mean_I);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    /*
    array_alloc(&var_I);
    array_sub(var_I, mean_II, tmp);
    */
   // Set arguments
    err = clSetKernelArg(kernel_mat_sub, 0, sizeof(cl_mem), &d_mean_II);
    err |= clSetKernelArg(kernel_mat_sub, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_sub, 2, sizeof(cl_mem), &d_var_I);
    err |= clSetKernelArg(kernel_mat_sub, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_sub, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

}

std::string readCLSourceCode(std::string source_file) {
    std::stringstream ss;
    std::ifstream file(source_file);
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            ss << line << std::endl;
        }
        file.close();
    }
    else {
        printf("File Not Found!\n");
        exit(0);
    }

    return ss.str();
}

void GuidedFilterMono::filterSingleChannel(float **result, float **p) const {

    d_P = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR, img_size, p, NULL);
    d_result = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_mean_p = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_tmp = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_mean_Ip = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_cov_Ip = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_a = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_b = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_mean_a = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);
    d_mean_b = clCreateBuffer(context, CL_MEM_READ_WRITE, img_size, NULL, NULL);

    // Create Execute Pipe
    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_p);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_P);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    // float **mean_p;
    // array_alloc(&mean_p);
    // array_boxfilter(mean_p, p, r);


    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_I);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_P);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    //float **tmp;
    //array_alloc(&tmp);
    //array_mul(tmp, I, p);

    
    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_Ip);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    // float **mean_Ip;
    // array_alloc(&mean_Ip);
    // array_boxfilter(mean_Ip, tmp, r);


    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_mean_I);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_mean_p);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    //array_mul(tmp, mean_I, mean_p);


    // Set arguments
    err = clSetKernelArg(kernel_mat_sub, 0, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_sub, 1, sizeof(cl_mem), &d_mean_Ip);
    err |= clSetKernelArg(kernel_mat_sub, 2, sizeof(cl_mem), &d_cov_Ip);
    err |= clSetKernelArg(kernel_mat_sub, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_sub, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_sub, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);    

    // float **cov_Ip;
    // array_alloc(&cov_Ip);
    // array_sub(cov_Ip, mean_Ip, tmp);  // this is the covariance of (I, p) in each local patch.


    // Set arguments
    err = clSetKernelArg(kernel_mat_add_const, 0, sizeof(cl_mem), &d_var_I);
    err |= clSetKernelArg(kernel_mat_add_const, 1, sizeof(float), &eps);
    err |= clSetKernelArg(kernel_mat_add_const, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_add_const, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_add_const, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_add_const, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);  

    // float **a;
    // array_alloc(&a);
    // array_add(tmp, var_I, eps);


    // Set arguments
    err = clSetKernelArg(kernel_mat_div, 0, sizeof(cl_mem), &d_cov_Ip);
    err |= clSetKernelArg(kernel_mat_div, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_div, 2, sizeof(cl_mem), &d_a);
    err |= clSetKernelArg(kernel_mat_div, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_div, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_div, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    //array_div(a, cov_Ip, tmp);  // Eqn. (5) in the paper;


    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_a);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_mean_I);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    //array_mul(tmp, a, mean_I);


    // Set arguments
    err = clSetKernelArg(kernel_mat_sub, 0, sizeof(cl_mem), &d_mean_p);
    err |= clSetKernelArg(kernel_mat_sub, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_sub, 2, sizeof(cl_mem), &d_b);
    err |= clSetKernelArg(kernel_mat_sub, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_sub, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_sub, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);    

    // float **b;
    // array_alloc(&b);
    // array_sub(b, mean_p, tmp);  // Eqn. (6) in the paper;

    
    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_a);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_a);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);

    // float **mean_a;
    // array_alloc(&mean_a);
    // array_boxfilter(mean_a, a, r);


    // Set arguments
    err = clSetKernelArg(kernel_boxfilter, 0, sizeof(cl_mem), &d_mean_b);
    err |= clSetKernelArg(kernel_boxfilter, 1, sizeof(cl_mem), &d_b);
    err |= clSetKernelArg(kernel_boxfilter, 2, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_boxfilter, 3, sizeof(int), &rows);
    err |= clSetKernelArg(kernel_boxfilter, 4, sizeof(int), &r);
    err |= clSetKernelArg(kernel_boxfilter, 5, sizeof(cl_mem), &d_boxtmp);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_boxfilter, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);    

    // float **mean_b;
    // array_alloc(&mean_b);
    // array_boxfilter(mean_b, b, r);

    
    // Set arguments
    err = clSetKernelArg(kernel_mat_mul, 0, sizeof(cl_mem), &d_mean_a);
    err |= clSetKernelArg(kernel_mat_mul, 1, sizeof(cl_mem), &d_I);
    err |= clSetKernelArg(kernel_mat_mul, 2, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_mul, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_mul, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_mul, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);
    
    //array_mul(tmp, mean_a, I);

    
    // Set arguments
    err = clSetKernelArg(kernel_mat_add_mat, 0, sizeof(cl_mem), &d_mean_b);
    err |= clSetKernelArg(kernel_mat_add_mat, 1, sizeof(cl_mem), &d_tmp);
    err |= clSetKernelArg(kernel_mat_add_mat, 2, sizeof(cl_mem), &d_result);
    err |= clSetKernelArg(kernel_mat_add_mat, 3, sizeof(int), &cols);
    err |= clSetKernelArg(kernel_mat_add_mat, 4, sizeof(int), &rows);

    // Execute the kernel
    err = clEnqueueNDRangeKernel(queue, kernel_mat_add_mat, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);    
    
    //array_add(result, tmp, mean_b);
    clFinish(queue);
    clEnqueueReadBuffer(queue, d_result, CL_TRUE, 0, img_size, result, 0, NULL, NULL);
}

GuidedFilter::GuidedFilter(float **I, int r, double eps) {
    std::string source_kernel = readCLSourceCode("guidedfilter.cl");
    const char* source_pt = source_kernel.c_str();
    size_t source_len = strlen(source_pt);

    err = clGetPlatformIDs(1, &cpPlatform, NULL); // bind to device
    err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL); // Get ID of devices( In this case it need a GPU device)
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err); // create context
    queue = clCreateCommandQueueWithProperties(context, device_id, NULL, &err); // create command queue
    program = clCreateProgramWithSource(context, 1, (const char **)&source_pt, &source_len, &err);
    err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL); // Build program executable
    if (err != CL_SUCCESS) {
        printf("cannot build program successfully!\n");
        size_t logSize;
        err = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,0, NULL, &logSize);
        char *log = (char*)malloc(logSize);
        err = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,logSize, log, NULL);
        printf("%s\n", log);
        exit(-1);
    }

    kernel_boxfilter = clCreateKernel(program, "boxfilter", &err);
    kernel_mat_mul = clCreateKernel(program, "mat_mul", &err);
    kernel_mat_div = clCreateKernel(program, "mat_div", &err);
    kernel_mat_sub = clCreateKernel(program, "mat_sub", &err);
    kernel_mat_add_mat = clCreateKernel(program, "mat_add_mat", &err);
    kernel_mat_add_const = clCreateKernel(program, "mat_add_const", &err);

    globalSize = ceil((cols * rows) / (float)localSize) * localSize;

    img_size = ( cols * rows ) * sizeof(float);
    
    impl_ = new GuidedFilterMono(I, 2 * r + 1, eps);
}

GuidedFilter::~GuidedFilter() {
    delete impl_;
}

void GuidedFilter::filter(float **output, float **p) const {
    impl_->filter(output, p);
}

cv::Mat guidedFilter(cv::Mat &I, cv::Mat &p, int r, double eps, int depth) {
    I = convertTo(I, CV_32F);
    p = convertTo(p, CV_32F);
    cols = I.cols;
    rows = I.rows;
    I_channels = I.channels();
    p_channels = p.channels();

    if (I_channels != 1 || p_channels != 1) {
        std::cerr << "Error, Full color images not suppported" << std::endl;
    }

    cv::Mat output;
    I.copyTo(output);

    // To Array
    float **arr_I, **arr_p, **arr_out;
    array_alloc(&arr_I);
    array_alloc(&arr_p);
    array_alloc(&arr_out);

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_I[i][j] = I.at<float>(i,j);
            arr_p[i][j] = p.at<float>(i,j);
        }
    }

    GuidedFilter(arr_I, r, eps).filter(arr_out, arr_p);

    // To Mat
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            output.at<float>(i,j)=arr_out[i][j];
        }
    }

    array_dealloc(&arr_I);
    array_dealloc(&arr_p);
    array_dealloc(&arr_out);

    releaseAllOpenCLObj();

    output = convertTo(output, depth == -1 ? CV_8U : depth);
    return output;
}
