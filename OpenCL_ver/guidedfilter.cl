__kernel void boxfilter(__global float* result, __global float* I, int cols, int rows, int r, __global float* tmp) {

    int global_id = get_global_id(0);

    int rf = r/2;
    int r_square= r*r;
    const int len = cols * rows;
    
    for(int i = global_id; i < rows; i+=get_global_id(0))
    {
        float now_sum = 0;
        for(int j = (0-rf); j < (0+rf+1); j++)  // initialize sum
        {
            float now_sum = 0;
		    for(int j = (0-rf); j < (0+rf+1); ++j)  // initialize sum
		    {
		        if(j<0)
		            now_sum += I[i*rows-j];
		        else
		            now_sum += I[i*rows+j];
		    }
		    tmp[i * rows] = now_sum;

		    for(int j = 1; j < rf+1; ++j)
		    {
		        now_sum = now_sum - I[i*rows-j+1+rf];
		        now_sum = now_sum + I[i*rows+j+rf];
		        tmp[i*rows + j] = now_sum;
		    }

		    for(int j = rf+1; j < cols-rf; ++j)
		    {
		        now_sum = now_sum - I[rows * i + j-1-rf];
		        now_sum = now_sum + I[rows * i + j+rf];
		        tmp[i*rows + j] = now_sum;
		    }

		    for(int j = cols-rf; j < cols; ++j)
		    {
		        now_sum = now_sum - I[i*rows + j-1-rf];
		        now_sum = now_sum + I[i*rows + 2*cols-j-rf-2];
		        tmp[i * rows + j] = now_sum;
		    }
        }
        tmp[i * rows] = now_sum;
        for(int j = 1; j < cols; j++)
        {
            float now_sum = 0;
		    for(int i = (0-rf); i < (0+rf+1); ++i)  // initialize sum
		    {
		        if(i<0)
		            now_sum += tmp[(-i) * rows + j ];
		        else
		            now_sum += tmp[i*rows + j];
		    }
		    result[j] = now_sum/r_square;
		    for(int i = 1; i < rf+1; ++i)
		    {
		        now_sum = now_sum - tmp[(-i+1+rf)*rows + j];
		        now_sum = now_sum + tmp[(i+rf)*rows + j ];
		        result[i*rows+j] = now_sum/r_square;
		    }
		    for(int i = rf+1; i < rows-rf; ++i)
		    {
		        now_sum = now_sum - tmp[(i-1-rf)*rows + j];
		        now_sum = now_sum + tmp[(i+rf)*rows + j];
		        result[i*rows + j] = now_sum/r_square;
		    }
		    for(int i = rows-rf; i < rows; ++i)
		    {
		        now_sum = now_sum - tmp[(i-1-rf) * rows + j];
		        now_sum = now_sum + tmp[(2*rows-i-rf-2)*rows+j];
		        result[i*rows+j] = now_sum/r_square;
		    }
        }
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    for(int j = global_id; j < cols; j+=get_global_id(0))
    {
        float now_sum = 0;
        for(int i = (0-rf); i < (0+rf+1); i++)  // initialize sum
        {
            if(i<0)
                now_sum += tmp[-i*rows + j];
            else if (i > (rows-1))
                now_sum += tmp[(2*rows-i-2)*rows + j];
            else
                now_sum += tmp[i*rows + j];
        }
        result[j] = now_sum/r_square;
        for(int i = 1; i < rows; i++)
        {
            int add_index, sub_index;
            add_index = i+rf;
            sub_index = i-1-rf;

            if(sub_index<0)
                now_sum = now_sum - tmp[(-sub_index)*rows+j];
            else if (sub_index > (rows-1))
                now_sum = now_sum - tmp[(2*rows-sub_index-2)*rows+j];
            else
                now_sum = now_sum - tmp[sub_index*rows+j];

            if(add_index<0)
                now_sum = now_sum + tmp[(-add_index)*rows + j];
            else if (add_index > (rows-1))
                now_sum = now_sum + tmp[(2*rows-add_index-2)*rows+j];
            else
                now_sum = now_sum + tmp[add_index *rows +j];
            result[i*rows+j] = now_sum/r_square;
        }
    }

}

__kernel void mat_mul(__global float* A, __global float* B, __global float* C, int col, int row) {
    int global_id = get_global_id(0);
    
    for ( int i = global_id ; i < (col * row) ; i += get_global_size(0) ) {
        C[i] = A[i] * B[i];
    }
}

__kernel void mat_div(__global float* A, __global float* B, __global float* C, int col, int row) {
    int global_id = get_global_id(0);

    for ( int i = global_id ; i < ( col * row ) ; i += get_global_size(0) ) {
        C[i] = A[i] / B[i];
    }
}

__kernel void mat_sub(__global float* A, __global float* B, __global float* C, int col, int row) {
    int global_id = get_global_id(0);

    for ( int i = global_id ; i < ( col * row ) ; i += get_global_size(0)) {
        C[i] = A[i] - B[i];
    } 
}

__kernel void mat_add_mat(__global float* A, __global float* B, __global float* C, int col, int row) {
    int global_id = get_global_id(0);

    for ( int i = global_id ; i < ( col * row ) ; i+=get_global_size(0) ) {
        C[i] = A[i] + B[i];
    }
}

__kernel void mat_add_const(__global float* A, float B, __global float* C, int col, int row) {
    int global_id = get_global_id(0);

    for ( int i = global_id ; i < ( col * row ) ; i += get_global_size(0) ) {
        C[i] = A[i] + B;
    }
}
