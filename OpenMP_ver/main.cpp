#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <chrono>
#include "guidedfilter.h"
typedef std::chrono::high_resolution_clock Clock;
using namespace std;

int main(int argc, char** argv) {
    if ( argc < 2 || argc > 2 ) {
        cout << "[Usage] ./gf <image path>" << endl;
        return EXIT_FAILURE;
    }

    char* img_path = argv[1];

    cv::Mat I = cv::imread(img_path, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat p = I;

    int r = 4;
    double eps = 0.2 * 0.2;

    eps *= 255 * 255;
    auto t1 = Clock::now();
    cv::Mat q = guidedFilter(I, p, r, eps);
    auto t2 = Clock::now();

    cout << "Guilded Filter Processing Time: " << chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " ms" << endl;

    cv::imwrite("result.jpg", q);

    return EXIT_SUCCESS;
}
