
static void mat_boxfilter(cv::Mat &cv_result, const cv::Mat &cv_I, int r)
{

//=============== To array =========================
    float **I = new float*[rows], **result = new float*[rows];
    for(int i = 0; i < rows; ++i)
    {
      I[i] = new float[cols];
      result[i] = new float[cols];
    }

    for(int j = 0 ;j < rows;j++)
    {
      for(int i = 0 ; i < cols; i++)
      {
        I[j][i] = cv_I.at<float>(j,i);
      }
    }
//========================================


    int rf = r/2;
    int a,b,r_square= r*r;
    float acc = 0;
    for (int j = 0 ;j < rows;j++)
    {
        for (int i = 0 ; i < cols; i++)
        {
            for(int l = (j-rf) ; l < (j+rf+1) ; l++)
            {
                for(int k = (i-rf) ; k < (i+rf+1) ; k++)
                {
                    if(k<0)
                        b = k*(-1);
                    else if (k > (cols-1))
                        b = 2*cols-k-2;
                    else
                        b = k;

                    if(l<0)
                        a = l*(-1);
                    else if (l > (rows-1))
                        a = 2*rows-l-2;
                    else
                        a = l;
                    //acc += I.at<float>(a,b);
                    acc += I[a][b];
                }
            }
            //result.at<float>(j,i)=acc/r_square;
            result[j][i] = acc/r_square;
            acc = 0;
        }
    }


//=============== To Mat =========================
    for(int j = 0 ;j < rows;j++)
    {
      for(int i = 0 ; i < cols; i++)
      {
        cv_result.at<float>(j,i)=result[j][i];
      }
    }

    for (int i = 0; i < rows; i++)
    {
      delete[] I[i]; // Delete columns
      delete[] result[i]; // Delete columns
    }
    delete[] I; // Delete Rows
    delete[] result; // Delete Rows
//========================================
}

static void mat_mul(cv::Mat &result, const cv::Mat &op_a, const cv::Mat &op_b)
{
  //std::cout << rows << " " << cols << std::endl;

  //=============== To array =========================
  float **arr_a = new float*[rows];
  float **arr_b = new float*[rows];
  float **arr_out = new float*[rows];

  for(int i = 0; i < rows; ++i)
  {
    arr_a[i] = new float[cols];
    arr_b[i] = new float[cols];
    arr_out[i] = new float[cols];
  }

  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_a[i][j] = op_a.at<float>(i,j);
      arr_b[i][j] = op_b.at<float>(i,j);
    }
  }
  //========================================


  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_out[i][j] = arr_a[i][j] * arr_b[i][j];
    }
  }


  //=============== To Mat =========================
  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      result.at<float>(i,j)=arr_out[i][j];
    }
  }

  for (int i = 0; i < rows; i++)
  {
    delete[] arr_a[i]; // Delete columns
    delete[] arr_b[i]; // Delete columns
    delete[] arr_out[i]; // Delete columns
  }
  delete[] arr_a; // Delete Rows
  delete[] arr_b; // Delete Rows
  delete[] arr_out; // Delete Rows
  //========================================
}

static void mat_div(cv::Mat &result, const cv::Mat &op_a, const cv::Mat &op_b)
{
  //=============== To array =========================
  float **arr_a = new float*[rows];
  float **arr_b = new float*[rows];
  float **arr_out = new float*[rows];

  for(int i = 0; i < rows; ++i)
  {
    arr_a[i] = new float[cols];
    arr_b[i] = new float[cols];
    arr_out[i] = new float[cols];
  }

  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_a[i][j] = op_a.at<float>(i,j);
      arr_b[i][j] = op_b.at<float>(i,j);
    }
  }
  //========================================


  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_out[i][j] = arr_a[i][j] / arr_b[i][j];
    }
  }

  //=============== To Mat =========================
  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      result.at<float>(i,j)=arr_out[i][j];
    }
  }

  for (int i = 0; i < rows; i++)
  {
    delete[] arr_a[i]; // Delete columns
    delete[] arr_b[i]; // Delete columns
    delete[] arr_out[i]; // Delete columns
  }
  delete[] arr_a; // Delete Rows
  delete[] arr_b; // Delete Rows
  delete[] arr_out; // Delete Rows
  //========================================
}


static void mat_sub(cv::Mat &result, const cv::Mat &op_a, const cv::Mat &op_b)
{
  //=============== To array =========================
  float **arr_a = new float*[rows];
  float **arr_b = new float*[rows];
  float **arr_out = new float*[rows];

  for(int i = 0; i < rows; ++i)
  {
    arr_a[i] = new float[cols];
    arr_b[i] = new float[cols];
    arr_out[i] = new float[cols];
  }

  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_a[i][j] = op_a.at<float>(i,j);
      arr_b[i][j] = op_b.at<float>(i,j);
    }
  }
  //========================================


  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_out[i][j] = arr_a[i][j] - arr_b[i][j];
    }
  }

  //=============== To Mat =========================
  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      result.at<float>(i,j)=arr_out[i][j];
    }
  }

  for (int i = 0; i < rows; i++)
  {
    delete[] arr_a[i]; // Delete columns
    delete[] arr_b[i]; // Delete columns
    delete[] arr_out[i]; // Delete columns
  }
  delete[] arr_a; // Delete Rows
  delete[] arr_b; // Delete Rows
  delete[] arr_out; // Delete Rows
  //========================================
}


static void mat_add(cv::Mat &result, const cv::Mat &op_a, const cv::Mat &op_b)
{
  //=============== To array =========================
  float **arr_a = new float*[rows];
  float **arr_b = new float*[rows];
  float **arr_out = new float*[rows];

  for(int i = 0; i < rows; ++i)
  {
    arr_a[i] = new float[cols];
    arr_b[i] = new float[cols];
    arr_out[i] = new float[cols];
  }

  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_a[i][j] = op_a.at<float>(i,j);
      arr_b[i][j] = op_b.at<float>(i,j);
    }
  }
  //========================================


  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_out[i][j] = arr_a[i][j] + arr_b[i][j];
    }
  }

  //=============== To Mat =========================
  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      result.at<float>(i,j)=arr_out[i][j];
    }
  }

  for (int i = 0; i < rows; i++)
  {
    delete[] arr_a[i]; // Delete columns
    delete[] arr_b[i]; // Delete columns
    delete[] arr_out[i]; // Delete columns
  }
  delete[] arr_a; // Delete Rows
  delete[] arr_b; // Delete Rows
  delete[] arr_out; // Delete Rows
  //========================================
}


static void mat_add(cv::Mat &result, const cv::Mat &op_a, float b)
{
  //=============== To array =========================
  float **arr_a = new float*[rows];
  float **arr_out = new float*[rows];

  for(int i = 0; i < rows; ++i)
  {
    arr_a[i] = new float[cols];
    arr_out[i] = new float[cols];
  }

  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_a[i][j] = op_a.at<float>(i,j);
    }
  }
  //========================================


  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      arr_out[i][j] = arr_a[i][j] + b;
    }
  }

  //=============== To Mat =========================
  for(int i = 0; i < rows; i++)
  {
    for(int j = 0; j < cols; j++)
    {
      result.at<float>(i,j)=arr_out[i][j];
    }
  }

  for (int i = 0; i < rows; i++)
  {
    delete[] arr_a[i]; // Delete columns
    delete[] arr_out[i]; // Delete columns
  }
  delete[] arr_a; // Delete Rows
  delete[] arr_out; // Delete Rows
  //========================================
}
