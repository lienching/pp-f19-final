cmake_minimum_required(VERSION 2.8.12)

project(guidedfilter)

set(CMAKE_CXX_STANDARD 11)
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_executable(gf main.cpp guidedfilter.cpp guidedfilter.h)
target_link_libraries(gf ${OpenCV_LIBS})
