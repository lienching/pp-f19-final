#ifndef GUIDED_FILTER_H
#define GUIDED_FILTER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <chrono>
#include <vector>
typedef std::chrono::high_resolution_clock Clock;

class GuidedFilterImpl;

class GuidedFilter
{
public:
    GuidedFilter(float **I, int r, double eps);
    ~GuidedFilter();

    void filter(float **output, float **p) const;

private:
    GuidedFilterImpl *impl_;
};

class GuidedFilterImpl {
public:
    virtual ~GuidedFilterImpl() {}

    void filter(float **output, float **p);

private:
    virtual void filterSingleChannel(float **result, float **p) const = 0;
};

class GuidedFilterMono : public GuidedFilterImpl {
public:
    GuidedFilterMono(float **I, int r, double eps);

private:
    virtual void filterSingleChannel(float **result, float **p) const;

private:
    int r;
    double eps;
    float **I, **mean_I, **var_I;
};
/*
class GuidedFilterColor : public GuidedFilterImpl {
public:
    GuidedFilterColor(const cv::Mat &I, int r, double eps);

private:
    virtual cv::Mat filterSingleChannel(const cv::Mat &p) const;

private:
    std::vector<cv::Mat> Ichannels;
    int r;
    double eps;
    cv::Mat mean_I_r, mean_I_g, mean_I_b;
    cv::Mat invrr, invrg, invrb, invgg, invgb, invbb;
};
*/
cv::Mat guidedFilter(cv::Mat &I, cv::Mat &p, int r, double eps, int depth = -1);

#endif
