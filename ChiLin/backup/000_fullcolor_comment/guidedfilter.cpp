#include "guidedfilter.h"

int cols;
int rows;
int I_channels; // 1: gray scale, 3: full color
int p_channels; // 1: gray scale, 3: full color

static void array_alloc(float*** arr_out)  // Please be noted, I don't implement dealloc function
{
    *arr_out = new float*[rows];
    for(int i = 0; i < rows; ++i)
    {
        (*arr_out)[i] = new float[cols];
    }
}

static void array_dealloc(float*** arr_out)
{
    for (int i = 0; i < rows; i++)
    {
        delete[] (*arr_out)[i];
    }
    delete[] (*arr_out);
}

static void array_boxfilter(float **result, float **I, int r)
{
    int rf = r/2;
    int a,b,r_square= r*r;
    float acc = 0;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            for(int k = (i-rf); k < (i+rf+1); k++)
            {
                if(k<0)
                    a = k*(-1);
                else if (k > (rows-1))
                    a = 2*rows-k-2;
                else
                    a = k;
                for(int l = (j-rf); l < (j+rf+1); l++)
                {
                    if(l<0)
                        b = l*(-1);
                    else if (l > (cols-1))
                        b = 2*cols-l-2;
                    else
                        b = l;
                    acc += I[a][b];
                }
            }
            result[i][j] = acc/r_square;
            acc = 0;
        }
    }
}

static void array_mul(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] * arr_b[i][j];
        }
    }
}

static void array_div(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] / arr_b[i][j];
        }
    }
}

static void array_sub(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] - arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + b;
        }
    }
}

static cv::Mat convertTo(const cv::Mat &mat, int depth) {
    if (mat.depth() == depth)
        return mat;

    cv::Mat result;
    mat.convertTo(result, depth);
    return result;
}


void GuidedFilterImpl::filter(float **output, float **p) {
    if (p_channels == 1)
    {
        filterSingleChannel(output, p);
    }
    /*else
    {
        std::vector<cv::Mat> pc;
        cv::split(p, pc);

        for (std::size_t i = 0; i < pc.size(); ++i)
            pc[i] = filterSingleChannel(pc[i]);

        cv::merge(pc, output);
    }*/
}

GuidedFilterMono::GuidedFilterMono(float **origI, int r, double eps) : r(r), eps(eps) {
    I = origI;

    array_alloc(&mean_I);
    array_boxfilter(mean_I, I, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, I);

    float **mean_II;
    array_alloc(&mean_II);
    array_boxfilter(mean_II, tmp, r);

    array_mul(tmp, mean_I, mean_I);

    array_alloc(&var_I);
    array_sub(var_I, mean_II, tmp);
}

void GuidedFilterMono::filterSingleChannel(float **result, float **p) const {
    float **mean_p;
    array_alloc(&mean_p);
    array_boxfilter(mean_p, p, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, p);

    float **mean_Ip;
    array_alloc(&mean_Ip);
    array_boxfilter(mean_Ip, tmp, r);

    array_mul(tmp, mean_I, mean_p);

    float **cov_Ip;
    array_alloc(&cov_Ip);
    array_sub(cov_Ip, mean_Ip, tmp);  // this is the covariance of (I, p) in each local patch.


    float **a;
    array_alloc(&a);
    array_add(tmp, var_I, eps);
    array_div(a, cov_Ip, tmp);  // Eqn. (5) in the paper;

    array_mul(tmp, a, mean_I);

    float **b;
    array_alloc(&b);
    array_sub(b, mean_p, tmp);  // Eqn. (6) in the paper;

    float **mean_a;
    array_alloc(&mean_a);
    array_boxfilter(mean_a, a, r);

    float **mean_b;
    array_alloc(&mean_b);
    array_boxfilter(mean_b, b, r);

    array_mul(tmp, mean_a, I);

    array_add(result, tmp, mean_b);
}

/*GuidedFilterColor::GuidedFilterColor(const cv::Mat &origI, int r, double eps) : r(r), eps(eps) {
    cv::Mat I;
    I = origI.clone();

    cv::split(I, Ichannels);

    mean_I_r = boxfilter(Ichannels[0], r);
    mean_I_g = boxfilter(Ichannels[1], r);
    mean_I_b = boxfilter(Ichannels[2], r);

    // variance of I in each local patch: the matrix Sigma in Eqn (14).
    // Note the variance in each local patch is a 3x3 symmetric matrix:
    //           rr, rg, rb
    //   Sigma = rg, gg, gb
    //           rb, gb, bb
    cv::Mat var_I_rr = boxfilter(Ichannels[0].mul(Ichannels[0]), r) - mean_I_r.mul(mean_I_r) + eps;
    cv::Mat var_I_rg = boxfilter(Ichannels[0].mul(Ichannels[1]), r) - mean_I_r.mul(mean_I_g);
    cv::Mat var_I_rb = boxfilter(Ichannels[0].mul(Ichannels[2]), r) - mean_I_r.mul(mean_I_b);
    cv::Mat var_I_gg = boxfilter(Ichannels[1].mul(Ichannels[1]), r) - mean_I_g.mul(mean_I_g) + eps;
    cv::Mat var_I_gb = boxfilter(Ichannels[1].mul(Ichannels[2]), r) - mean_I_g.mul(mean_I_b);
    cv::Mat var_I_bb = boxfilter(Ichannels[2].mul(Ichannels[2]), r) - mean_I_b.mul(mean_I_b) + eps;

    // Inverse of Sigma + eps * I
    invrr = var_I_gg.mul(var_I_bb) - var_I_gb.mul(var_I_gb);
    invrg = var_I_gb.mul(var_I_rb) - var_I_rg.mul(var_I_bb);
    invrb = var_I_rg.mul(var_I_gb) - var_I_gg.mul(var_I_rb);
    invgg = var_I_rr.mul(var_I_bb) - var_I_rb.mul(var_I_rb);
    invgb = var_I_rb.mul(var_I_rg) - var_I_rr.mul(var_I_gb);
    invbb = var_I_rr.mul(var_I_gg) - var_I_rg.mul(var_I_rg);

    cv::Mat covDet = invrr.mul(var_I_rr) + invrg.mul(var_I_rg) + invrb.mul(var_I_rb);

    invrr /= covDet;
    invrg /= covDet;
    invrb /= covDet;
    invgg /= covDet;
    invgb /= covDet;
    invbb /= covDet;
}

cv::Mat GuidedFilterColor::filterSingleChannel(const cv::Mat &p) const {
    cv::Mat mean_p = boxfilter(p, r);

    cv::Mat mean_Ip_r = boxfilter(Ichannels[0].mul(p), r);
    cv::Mat mean_Ip_g = boxfilter(Ichannels[1].mul(p), r);
    cv::Mat mean_Ip_b = boxfilter(Ichannels[2].mul(p), r);

    // covariance of (I, p) in each local patch.
    cv::Mat cov_Ip_r = mean_Ip_r - mean_I_r.mul(mean_p);
    cv::Mat cov_Ip_g = mean_Ip_g - mean_I_g.mul(mean_p);
    cv::Mat cov_Ip_b = mean_Ip_b - mean_I_b.mul(mean_p);

    cv::Mat a_r = invrr.mul(cov_Ip_r) + invrg.mul(cov_Ip_g) + invrb.mul(cov_Ip_b);
    cv::Mat a_g = invrg.mul(cov_Ip_r) + invgg.mul(cov_Ip_g) + invgb.mul(cov_Ip_b);
    cv::Mat a_b = invrb.mul(cov_Ip_r) + invgb.mul(cov_Ip_g) + invbb.mul(cov_Ip_b);

    cv::Mat b = mean_p - a_r.mul(mean_I_r) - a_g.mul(mean_I_g) - a_b.mul(mean_I_b); // Eqn. (15) in the paper;

    return (boxfilter(a_r, r).mul(Ichannels[0])
          + boxfilter(a_g, r).mul(Ichannels[1])
          + boxfilter(a_b, r).mul(Ichannels[2])
          + boxfilter(b, r));  // Eqn. (16) in the paper;
}
*/

GuidedFilter::GuidedFilter(float **I, int r, double eps) {
    if (I_channels == 1)
        impl_ = new GuidedFilterMono(I, 2 * r + 1, eps);
    /*else
        impl_ = new GuidedFilterColor(I, 2 * r + 1, eps);*/
}

GuidedFilter::~GuidedFilter() {
    delete impl_;
}

void GuidedFilter::filter(float **output, float **p) const {
    impl_->filter(output, p);
}

cv::Mat guidedFilter(cv::Mat &I, cv::Mat &p, int r, double eps, int depth) {
    I = convertTo(I, CV_32F);
    p = convertTo(p, CV_32F);
    cols = I.cols;
    rows = I.rows;
    I_channels = I.channels();
    p_channels = p.channels();

    cv::Mat output;
    I.copyTo(output);

    // To Array
    float **arr_I, **arr_p, **arr_out;
    array_alloc(&arr_I);
    array_alloc(&arr_p);
    array_alloc(&arr_out);

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_I[i][j] = I.at<float>(i,j);
            arr_p[i][j] = p.at<float>(i,j);
        }
    }

    GuidedFilter(arr_I, r, eps).filter(arr_out, arr_p);

    // To Mat
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            output.at<float>(i,j)=arr_out[i][j];
        }
    }

    array_dealloc(&arr_I);
    array_dealloc(&arr_p);
    array_dealloc(&arr_out);

    output = convertTo(output, depth == -1 ? CV_8U : depth);
    return output;
}
