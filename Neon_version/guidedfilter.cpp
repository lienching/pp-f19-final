#include <omp.h>
#include "guidedfilter.h"
#include <vector>

std::vector<float> cache;
std::vector<float> colSum;

int cols;
int rows;
int I_channels; // 1: gray scale, 3: full color
int p_channels; // 1: gray scale, 3: full color

// Please be noted, I don't apply array_dealloc for every array_alloc, and that may cause memory leak

static void array_alloc(float*** arr_out)
{
    *arr_out = new float*[rows];
    for(int i = 0; i < rows; ++i)
    {
        (*arr_out)[i] = new float[cols];
    }
}

static void array_dealloc(float*** arr_out)
{
    for (int i = 0; i < rows; i++)
    {
        delete[] (*arr_out)[i];
    }
    delete[] (*arr_out);
}

static void array_boxfilter(float **result, float **I, int r)
{

  float *input = (float*)malloc(sizeof(float)*(cols*rows));
  float *output = (float*)malloc(sizeof(float)*(cols*rows));

  for ( int i = 0 ; i < rows ; i++ ) {
      for ( int j = 0 ; j < cols ; j++ ) {
          input[i*rows + j] = I[i][j];
      }
  }
  
    float *cachePtr = &(cache[0]);
  // sum horizonal
  for (int h = 0; h < rows; ++h) {
    int shift = h * cols;

    float tmp = 0;
    for (int w = 0; w < r; ++w) {
      tmp += input[shift + w];
    }

    for (int w = 0; w <= r; ++w) {
      tmp += input[shift + w + r];
      cachePtr[shift + w] = tmp;
    }

    int start = r + 1;
    int end = cols - 1 - r;
    for (int w = start; w <= end; ++w) {
      tmp += input[shift + w + r];
      tmp -= input[shift + w - r - 1];
      cachePtr[shift + w] = tmp;
    }

    start = cols - r;
    for (int w = start; w < cols; ++w) {
      tmp -= input[shift + w - r - 1];
      cachePtr[shift + w] = tmp;
    }
  }

  float *colSumPtr = &(colSum[0]);

  for (int indexW = 0; indexW < cols; ++indexW) {
    colSumPtr[indexW] = 0;
  } 

  int n = cols >> 2;
  int re = cols - (n << 2);
  // sum vertical
  for (int h = 0; h < r; ++h) {
    int shift = h * cols;

    float *tmpColSumPtr = colSumPtr;
    float *tmpCachePtr = cachePtr + shift;
    int indexW = 0;

    int nn = n;
    int remain = re;
#if __ARM_NEON
    for (; nn > 0; nn--) {
      float32x4_t _colSum = vld1q_f32(tmpColSumPtr);
      float32x4_t _cache = vld1q_f32(tmpCachePtr);

      float32x4_t _tmp = vaddq_f32(_colSum, _cache);

      vst1q_f32(tmpColSumPtr, _tmp); 

      tmpColSumPtr += 4;
      tmpCachePtr += 4;
    }
#endif // __ARM_NEON
    for (; remain > 0; --remain) {
      *tmpColSumPtr += *tmpCachePtr;
      tmpColSumPtr ++;
      tmpCachePtr ++;
    }
  }

  for (int h = 0; h <= r; ++h) {
    float *addPtr = cachePtr + (h + r) * cols;
    int shift = h * cols;
    float *outPtr = output + shift; 
    int indexW = 0;

    float *tmpOutPtr = outPtr;
    float *tmpColSumPtr = colSumPtr;
    float *tmpAddPtr = addPtr;

    int nn = n;
    int remain = re;
#if __ARM_NEON
    for (; nn > 0; nn--) {
      float32x4_t _add = vld1q_f32(tmpAddPtr);
      float32x4_t _colSum = vld1q_f32(tmpColSumPtr);

      float32x4_t _tmp = vaddq_f32(_colSum, _add);

      vst1q_f32(tmpColSumPtr, _tmp);
      vst1q_f32(tmpOutPtr, _tmp);

      tmpAddPtr += 4;
      tmpColSumPtr += 4;
      tmpOutPtr += 4;
    }
#endif // __ARM_NEON
    for (; remain > 0; --remain) {
      *tmpColSumPtr += *tmpAddPtr;
      *tmpOutPtr = *tmpColSumPtr;
      tmpAddPtr ++;
      tmpColSumPtr ++;
      tmpOutPtr ++;
    }

  }

  int start = r + 1;
  int end = rows - 1 - r;
  for (int h = start; h <= end; ++h) {
    float *addPtr = cachePtr + (h + r) * cols;
    float *subPtr = cachePtr + (h - r - 1) * cols;
    int shift = h * cols;
    float *outPtr = output + shift; 
    int indexW = 0;
    float *tmpOutPtr = outPtr;
    float *tmpColSumPtr = colSumPtr;
    float *tmpAddPtr = addPtr;
    float *tmpSubPtr = subPtr;

    int nn = n;
    int remain = re;
#if __ARM_NEON
    for (; nn > 0; nn--) {
      float32x4_t _add = vld1q_f32(tmpAddPtr);
      float32x4_t _sub = vld1q_f32(tmpSubPtr);
      float32x4_t _colSum = vld1q_f32(tmpColSumPtr);

      float32x4_t _tmp = vaddq_f32(_colSum, _add);
      _tmp = vsubq_f32(_tmp, _sub);

      vst1q_f32(tmpColSumPtr, _tmp);
      vst1q_f32(tmpOutPtr, _tmp);

      tmpAddPtr += 4;
      tmpSubPtr += 4;
      tmpColSumPtr += 4;
      tmpOutPtr += 4;
    }
#endif // __ARM_NEON
    for (; remain > 0; --remain) {
      *tmpColSumPtr += *tmpAddPtr;
      *tmpColSumPtr -= *tmpSubPtr;
      *tmpOutPtr = *tmpColSumPtr;
      tmpAddPtr ++;
      tmpColSumPtr ++;
      tmpOutPtr ++;
      tmpSubPtr ++;
    }
  }

  start = rows - r;
  for (int h = start; h < rows; ++h) {
    float *subPtr = cachePtr + (h - r - 1) * cols;
    int shift = h * cols;
    float *outPtr = output + shift; 

    int indexW = 0;
    float *tmpOutPtr = outPtr;
    float *tmpColSumPtr = colSumPtr;
    float *tmpSubPtr = subPtr;

    int nn = n;
    int remain = re;
#if __ARM_NEON
    for (; nn > 0; nn--) {
      float32x4_t _sub = vld1q_f32(tmpSubPtr);
      float32x4_t _colSum = vld1q_f32(tmpColSumPtr);

      float32x4_t _tmp = vsubq_f32(_colSum, _sub);

      vst1q_f32(tmpColSumPtr, _tmp);
      vst1q_f32(tmpOutPtr, _tmp);

      tmpSubPtr += 4;
      tmpColSumPtr += 4;
      tmpOutPtr += 4;
    }
#endif // __ARM_NEON
    for (; remain > 0; --remain) {
      *tmpColSumPtr -= *tmpSubPtr;
      *tmpOutPtr = *tmpColSumPtr;
      tmpColSumPtr ++;
      tmpOutPtr ++;
      tmpSubPtr ++;
    }
  }

  for ( int i = 0 ; i < rows ; i++ ) {
      for ( int j = 0 ; j < cols ; j++ ) {
          result[i][j] = output[i*rows + j];
      }
  }
}

static void array_mul(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] * arr_b[i][j];
        }
    }
}

static void array_div(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] / arr_b[i][j];
        }
    }
}

static void array_sub(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] - arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float** arr_b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + arr_b[i][j];
        }
    }
}

static void array_add(float** arr_out, float** arr_a, float b)
{
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_out[i][j] = arr_a[i][j] + b;
        }
    }
}

static cv::Mat convertTo(const cv::Mat &mat, int depth) {
    if (mat.depth() == depth)
        return mat;

    cv::Mat result;
    mat.convertTo(result, depth);
    return result;
}


void GuidedFilterImpl::filter(float **output, float **p) {
    filterSingleChannel(output, p);
}

GuidedFilterMono::GuidedFilterMono(float **origI, int r, double eps) : r(r), eps(eps) {
    I = origI;

    array_alloc(&mean_I);
    array_boxfilter(mean_I, I, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, I);

    float **mean_II;
    array_alloc(&mean_II);
    array_boxfilter(mean_II, tmp, r);

    array_mul(tmp, mean_I, mean_I);

    array_alloc(&var_I);
    array_sub(var_I, mean_II, tmp);
}

void GuidedFilterMono::filterSingleChannel(float **result, float **p) const {
    float **mean_p;
    array_alloc(&mean_p);
    array_boxfilter(mean_p, p, r);

    float **tmp;
    array_alloc(&tmp);
    array_mul(tmp, I, p);

    float **mean_Ip;
    array_alloc(&mean_Ip);
    array_boxfilter(mean_Ip, tmp, r);

    array_mul(tmp, mean_I, mean_p);

    float **cov_Ip;
    array_alloc(&cov_Ip);
    array_sub(cov_Ip, mean_Ip, tmp);  // this is the covariance of (I, p) in each local patch.


    float **a;
    array_alloc(&a);
    array_add(tmp, var_I, eps);
    array_div(a, cov_Ip, tmp);  // Eqn. (5) in the paper;

    array_mul(tmp, a, mean_I);

    float **b;
    array_alloc(&b);
    array_sub(b, mean_p, tmp);  // Eqn. (6) in the paper;

    float **mean_a;
    array_alloc(&mean_a);
    array_boxfilter(mean_a, a, r);

    float **mean_b;
    array_alloc(&mean_b);
    array_boxfilter(mean_b, b, r);

    array_mul(tmp, mean_a, I);

    array_add(result, tmp, mean_b);
}

GuidedFilter::GuidedFilter(float **I, int r, double eps) {
    impl_ = new GuidedFilterMono(I, 2 * r + 1, eps);
}

GuidedFilter::~GuidedFilter() {
    delete impl_;
}

void GuidedFilter::filter(float **output, float **p) const {
    impl_->filter(output, p);
}

cv::Mat guidedFilter(cv::Mat &I, cv::Mat &p, int r, double eps, int depth) {
    I = convertTo(I, CV_32F);
    p = convertTo(p, CV_32F);
    cols = I.cols;
    rows = I.rows;
    I_channels = I.channels();
    p_channels = p.channels();

    if (I_channels != 1 || p_channels != 1) {
        std::cerr << "Error, Full color images not suppported" << std::endl;
    }

    cv::Mat output;
    I.copyTo(output);

    // To Array
    float **arr_I, **arr_p, **arr_out;
    array_alloc(&arr_I);
    array_alloc(&arr_p);
    array_alloc(&arr_out);

    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            arr_I[i][j] = I.at<float>(i,j);
            arr_p[i][j] = p.at<float>(i,j);
        }
    }

    GuidedFilter(arr_I, r, eps).filter(arr_out, arr_p);

    // To Mat
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            output.at<float>(i,j)=arr_out[i][j];
        }
    }

    array_dealloc(&arr_I);
    array_dealloc(&arr_p);
    array_dealloc(&arr_out);

    output = convertTo(output, depth == -1 ? CV_8U : depth);
    return output;
}
